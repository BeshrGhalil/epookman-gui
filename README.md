# epookman-gui
An ebook manager written in Python.

# Requirements
- [Python3](https://www.python.org/download/releases/3.0/)
- [SQLite3](https://sqlite.org/index.html)
- [Poppler](https://poppler.freedesktop.org/)

# Install
## Build from source
```
git clone https://github.com/BishrGhalil/epookman-gui.git
cd epookman-gui
sudo make install
```

# Features
- Simple and beautiful interface
- Multiple themes
- Auto detect ebook formats using mime types
- Parallel scanning

# Screenshots
![dark purple theme](https://raw.githubusercontent.com/BishrGhalil/epookman-gui/master/screenshots/dark-purple.png)
![dark yellow theme](https://raw.githubusercontent.com/BishrGhalil/epookman-gui/master/screenshots/dark-yellow.png)
![light red theme](https://raw.githubusercontent.com/BishrGhalil/epookman-gui/master/screenshots/light-red.png)
![black and white](https://raw.githubusercontent.com/BishrGhalil/epookman-gui/master/screenshots/black&white.png)
![white and black](https://raw.githubusercontent.com/BishrGhalil/epookman-gui/master/screenshots/white&black.png)
![ebook list](https://raw.githubusercontent.com/BishrGhalil/epookman-gui/master/screenshots/ebook-list.png)
